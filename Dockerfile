FROM node:6-slim

MAINTAINER Chris <36web.rocks@gmail.com>

ARG VERSION=3.2.3

LABEL version=$VERSION

RUN npm install --global gitbook-cli \
	&& gitbook fetch ${VERSION} \
	&& npm cache clear \
	&& rm -rf /tmp/*

# Install calibre for export to pdf and ttf-wqy-microhei for Chinese fonts
RUN apt-get update \
	&& DEBIAN_FRONTEND=noninteractive apt-get install -y -q --no-install-recommends \
		calibre \
		ttf-wqy-microhei \
	&& rm -rf /var/lib/apt/lists/*

WORKDIR /gitbook

VOLUME /gitbook

EXPOSE 4000 35729

CMD /usr/local/bin/gitbook serve
